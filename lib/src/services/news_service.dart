import 'package:flutter/material.dart';
import 'package:newsapp/src/models/news_models.dart';
import 'package:http/http.dart' as http;

final _baseUrl = 'http://newsapi.org/v2';
final _apiKey = 'dcb72943c10c425d9b4497196ea1a934';

class NewsService with ChangeNotifier {
  List<Article> headlines = [];

  NewsService() {
    this.getTopHeadlines();
  }

  getTopHeadlines() async {
    final url = '$_baseUrl/top-headlines?apiKey=$_apiKey&country=mx';
    final resp = await http.get(url);

    final newsResponse = newResponseFromJson(resp.body);

    this.headlines.addAll(newsResponse.articles);

    notifyListeners();
  }
}
